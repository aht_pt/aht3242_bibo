<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

		</div><!-- .site-content -->
		
		<footer id="colophon" class="site-footer" role="contentinfo">
			<div class="site-info">
				<div class="wrap">
					<?php if ( is_active_sidebar( 'sidebar-2' )  ) : ?>
						<?php dynamic_sidebar( 'sidebar-2' ); ?>
					<?php endif; ?>
				</div><!-- .WWrap -->
			</div><!-- .site-info -->
		</footer><!-- .site-footer -->
	</div><!-- .site-inner -->
</div><!-- .site -->

<?php wp_footer(); ?>
</body>
</html>
