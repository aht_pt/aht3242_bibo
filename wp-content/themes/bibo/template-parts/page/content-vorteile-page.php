<?php
/**
 * Template part for displaying page content in ihre-vorteile-page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
	<?php if(get_field('sub_title')) { ?>
		<h1 class="entry-title"><?php echo get_field('sub_title'); ?></h1>
	<?php }else { ?>
		<h1 class="entry-title"><?php echo get_the_title(); ?></h1>
	<?php } ?>
		<?php if(get_field('sub_title')) { ?>
			<div class="desc-wrap"><?php echo get_field('description_page'); ?></div>
		<?php } ?>
	</header><!-- .entry-header -->
	<div class="entry-content">
		<?php
			the_content();

			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'twentyseventeen' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->
</article><!-- #post-## -->
<?php if(get_field('extra_content')) { ?>
	<div class="extra-content"><?php echo get_field('extra_content'); ?></div>
<?php } ?>
