<?php
/**
 * Displays content for front page
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>
<article id="post-<?php the_ID(); ?>" <?php post_class( 'twentyseventeen-panel ' ); ?> >

	<?php if ( has_post_thumbnail() ) :
		$thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'twentyseventeen-featured-image' );

		$post_thumbnail_id = get_post_thumbnail_id( $post->ID );

		$thumbnail_attributes = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'twentyseventeen-featured-image' );

		// Calculate aspect ratio: h / w * 100%.
		$ratio = $thumbnail_attributes[2] / $thumbnail_attributes[1] * 100;
		?>

		<div class="panel-image" style="background-image: url(<?php echo esc_url( $thumbnail[0] ); ?>);">
			<div class="panel-image-prop" style="padding-top: <?php echo esc_attr( $ratio ); ?>%"></div>
		</div><!-- .panel-image -->

	<?php endif; ?>
	<?php if(get_field('slider_page')) { ?>
		<div class="banner-image" >
			<?php echo do_shortcode(get_field('slider_page')); ?>	
		</div>
	<?php } ?>
	<div class="panel-content">
		<div class="wrap">
			<header class="entry-header">
				<h2 class="entry-title"><?php echo get_field('sub_title'); ?></h2>
				<?php if(get_field('sub_title')) { ?>
					<div class="desc-wrap"><?php echo get_field('description_page'); ?></div>
				<?php } ?>
			</header><!-- .entry-header -->

			<div class="entry-content">
				<?php
					/* translators: %s: Name of current post */
					the_content( sprintf(
						__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'twentyseventeen' ),
						get_the_title()
					) );
				?>
			</div><!-- .entry-content -->
			<?php if ( is_active_sidebar( 'content-home' )  ) : ?>
				<div class="home-block">
					<?php dynamic_sidebar( 'content-home' ); ?>
				</div>
			<?php endif; ?>
		</div><!-- .wrap -->
	</div><!-- .panel-content -->

</article><!-- #post-## -->
