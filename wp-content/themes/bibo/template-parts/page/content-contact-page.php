<?php
/**
 * Template part for displaying page content in contact-page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>> 
	<header class="entry-header">
	<?php if(get_field('sub_title')) { ?>
		<h1 class="entry-title"><?php echo get_field('sub_title'); ?></h1>
	<?php }else { ?>
		<h1 class="entry-title"><?php echo get_the_title(); ?></h1>
	<?php } ?>
		<?php if(get_field('sub_title')) { ?>
			<div class="desc-wrap"><?php echo get_field('description_page'); ?></div>
		<?php } ?>
	</header><!-- .entry-header -->
	<div class="entry-content entry-content-contact">
		<div class="form-left">
		<?php
			the_content();

			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'twentyseventeen' ),
				'after'  => '</div>',
			) );
		?>
		</div>
		<div class="adresse-right">
		<?php echo get_field('adresse_text'); ?>
		</div>
	</div><!-- .entry-content -->
</article><!-- #post-## -->
<div class="bottom-map posts-content">
	<div class="map-content">
		<?php if(get_field('map_title')) { ?>
			<h4><?php echo get_field('map_title'); ?></h4>
		<?php } ?>
		<?php if(get_field('embed_maps')) { ?>
			<div class="map"><?php echo get_field('embed_maps'); ?></div>
		<?php } ?>
	<?php /*if(get_field('embed_maps')) { ?>
		<div class="map"><?php echo get_field('embed_maps'); ?></div>
	<?php }*/ ?>
	</div>
</div>
