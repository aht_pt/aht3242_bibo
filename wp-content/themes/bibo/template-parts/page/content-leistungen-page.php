<?php
/**
 * Template part for displaying page content in unsere-leistungen-page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
	<?php if(get_field('sub_title')) { ?>
		<h1 class="entry-title"><?php echo get_field('sub_title'); ?></h1>
	<?php }else { ?>
		<h1 class="entry-title"><?php echo get_the_title(); ?></h1>
	<?php } ?>
		<?php if(get_field('sub_title')) { ?>
			<div class="desc-wrap"><?php echo get_field('description_page'); ?></div>
		<?php } ?>
	</header><!-- .entry-header -->
	<div class="entry-content">
		<?php
			the_content();

			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'twentyseventeen' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->
</article><!-- #post-## -->

<?php 
	$poststorbau = get_posts( $args ); 
	$args = array(
		'post_type'      => 'leistungen',
		'posts_per_page' => -1,
		'order'          => 'DESC',
		'orderby'        => 'date'
	 );
	$postChild = new WP_Query( $args );
?>
<div class="posts-content container">
	<div class="post-list">
		
	<?php 
	if ( $postChild->have_posts() ) : 
	$i = -1; 
	?>

	<?php while ( $postChild->have_posts() ) : $postChild->the_post(); 				
		  $i++; 
	?>
		<div class="item" id="<?php echo $post->post_name; ?>">
			<?php if ( has_post_thumbnail() ) : ?>
			<div class="image-post">
				<h4 class="title"><span><?php the_title(); ?></span></h4>
				<div class="main-image">
					<?php the_post_thumbnail(); ?>
				</div>
				<?php if( have_rows('gallery') ): $i = 2;?>
				<div class="gallery-image">
				<?php while( have_rows('gallery') ) : the_row();
					$i++;
					$image = get_sub_field('image');
				?>
					<a href="<?php echo $image ?>" title="<?php the_title(); ?>" style="background-image: url(<?php echo $image ?>);"></a>
				<?php endwhile;	?>
				</div>
				<?php endif; ?>
			</div>
			<?php endif; ?>  
		</div>
		
	<?php endwhile; ?>
	<?php endif; wp_reset_query(); ?>
	</div>
</div>

<?php if( have_rows('block') ): $i = 0;?>
<div class="block-leistungen">
	<div class="block-list">
		<?php while( have_rows('block') ) : the_row();
			$i++;
			$title = get_sub_field('title_block');
			$description = get_sub_field('description_block');
		?>
			<div class="widget-item">
				<h4><?php echo $title; ?></h4>
				<div class="desc"><?php echo $description; ?></div>
			</div>
		<?php endwhile;	?>
	</div>
</div>
<?php endif; ?>
